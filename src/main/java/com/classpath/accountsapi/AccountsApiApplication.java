package com.classpath.accountsapi;

import com.classpath.accountsapi.util.HelloWorld;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class AccountsApiApplication {//implements CommandLineRunner {

	private final ApplicationContext applicationContext;
	private final HelloWorld helloWorld;

	public AccountsApiApplication(ApplicationContext applicationContext, HelloWorld helloWorld){
		this.applicationContext = applicationContext;
		this.helloWorld = helloWorld;
	}

	public static void main(String[] args) {
		SpringApplication.run(AccountsApiApplication.class, args);
	}

	//@Override
	public void run(String... args) throws Exception {
		System.out.println("=================================== Inside the run method of Accounts API application ===================================");
		final String[] beanDefinitionNames = this.applicationContext.getBeanDefinitionNames();
		for(String beanName: beanDefinitionNames){
			if ( beanName.startsWith("hello")) {
				System.out.println("Bean Name -  " + beanName);
			}
		}
		System.out.println("=================================== Inside the run method of Accounts API application ===================================");
//		System.out.println(this.helloWorld.greet("Prashanth ", "Hello World from Spring Boot"));
	}
}
