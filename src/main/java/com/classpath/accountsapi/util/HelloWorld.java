package com.classpath.accountsapi.util;

public class HelloWorld {

    public HelloWorld() {
        System.out.println(" Inside the constructor of Hello World");
    }

    public String greet(String firstName, String greetMessage){
        return firstName + " !! " + greetMessage;
    }
}