package com.classpath.accountsapi.uber;

public interface Commute {
     void commute(String from, String destination, String notes);
}