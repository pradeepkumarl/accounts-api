package com.classpath.accountsapi.uber;

import com.classpath.accountsapi.util.HelloWorld;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class UberClient  {


    private Customer customer;

    private HelloWorld helloWorld;

    public UberClient( Customer customer, HelloWorld helloWorld){
        this.customer = customer;
        this.helloWorld = helloWorld;
    }


    public void run(String... args) throws Exception {
        this.customer.travel("Bangalore", "Mysuru");
        System.out.println("Hello world dependency :: "+ helloWorld);
    }
}