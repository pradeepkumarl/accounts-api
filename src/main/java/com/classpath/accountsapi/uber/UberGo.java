package com.classpath.accountsapi.uber;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Primary
public class UberGo implements Commute {

    @Override
    public void commute(String from, String destination, String notes) {
        System.out.println("Commuting with UberGo from :: "+from + " to " + destination );
    }
}