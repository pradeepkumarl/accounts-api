package com.classpath.accountsapi.uber;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("prod")
public class UberPrime implements Commute{
    @Override
    public void commute(String from, String destination, String notes) {
        System.out.println("Travelling by Uber Prime from :: "+from + " destination "+ destination);
    }
}