package com.classpath.accountsapi.uber;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("test")
public class DummyUber implements Commute{
    @Override
    public void commute(String from, String destination, String notes) {
        System.out.println("Dummy implementation");
    }
}