package com.classpath.accountsapi.uber;

import org.springframework.stereotype.Component;

@Component
public class Customer {

    private Commute commute;

    public Customer(Commute commute){
        this.commute = commute;
    }

    public void travel(String from, String to){
        this.commute.commute(from, to, "Travelling with Uber");
    }
}