package com.classpath.accountsapi.controller;

import com.classpath.accountsapi.exception.InsufficientBalanceException;
import com.classpath.accountsapi.model.Transaction;
import com.classpath.accountsapi.service.AccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.NO_CONTENT;

@RestController
@RequestMapping("/api/accounts")
@RequiredArgsConstructor
@Slf4j
public class SavingsAccountController {

    private final AccountService accountService;

    @GetMapping("/statement/{id}")
    public Transaction statement(@PathVariable("id") long accountId){
        log.info(" Sending the statement :: ");
        return this.accountService.checkBalance(accountId);
    }

    @PutMapping("/deposit/{id}")
    @ResponseStatus(NO_CONTENT)
    public void deposit(@PathVariable("id") long accountId, @Valid @RequestBody Transaction transaction){

        this.accountService.deposit(accountId, transaction.getAmount());
    }

    @PutMapping("/withdraw/{id}")
    public double withdraw(@PathVariable("id") long accountId, @Valid @RequestBody Transaction transaction) throws InsufficientBalanceException {
        return this.accountService.withdraw(accountId, transaction.getAmount());
    }
}