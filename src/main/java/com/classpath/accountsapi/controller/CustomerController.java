package com.classpath.accountsapi.controller;

import com.classpath.accountsapi.model.Customer;
import com.classpath.accountsapi.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Set;

@RestController
@RequestMapping("/api/customers")
@RequiredArgsConstructor
public class CustomerController {

    private final CustomerService customerService;

    @PostMapping
    public Customer saveCustomer(@RequestBody @Valid Customer customer){
        customer.getAddressSet().forEach(address -> address.setCustomer(customer));
        customer.getSavingsAccount().setCustomer(customer);

        return this.customerService.save(customer);
    }

    @GetMapping
    public Set<Customer> fetchAll(){
        return this.customerService.fetchAllCustomers();
    }

    @GetMapping("/{id}")
    public Customer fetchById(@PathVariable("id") long customerId){
        return this.customerService.findCustomerById(customerId);
    }

    @DeleteMapping("/{id}")
    public void deleteCustomerById(@PathVariable long id){
        this.customerService.deleteCustomer(id);
    }
}