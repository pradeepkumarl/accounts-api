package com.classpath.accountsapi.service;

import com.classpath.accountsapi.model.Customer;
import com.classpath.accountsapi.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerRepository customerRepository;

    public Customer save(Customer customer){
        return this.customerRepository.save(customer);
    }

    public Set<Customer> fetchAllCustomers(){
        return new HashSet<>(this.customerRepository.findAll());
    }

    public Customer findCustomerById(long customerId){

        return this.customerRepository
                        .findById(customerId)
                        .orElseThrow(() -> new IllegalArgumentException("Invalid customer id"));
        // var ref = new Cat();
        // var fun = (x) =>  x.toUpperCase();
        // public void method(Cat cat){}
        // public Cat method(){}
    }

    public void deleteCustomer(long customerId){
        this.customerRepository.deleteById(customerId);
    }
}