package com.classpath.accountsapi.service;

import com.classpath.accountsapi.exception.InsufficientBalanceException;
import com.classpath.accountsapi.model.SavingsAccount;
import com.classpath.accountsapi.model.Transaction;
import com.classpath.accountsapi.repository.SavingsAccountRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

@Service
@AllArgsConstructor
public class AccountService {

    private SavingsAccountRepository savingsAccountRepository;

    public double withdraw(long accountId, double amount) throws InsufficientBalanceException {
        final SavingsAccount savingsAccount = this.savingsAccountRepository
                .findById(accountId).orElseThrow(() -> new IllegalArgumentException(" Invalid Savings account id"));

        double accountBalance = savingsAccount.getAccountBalance();
        if (amount < (accountBalance - amount)) {
            accountBalance -= amount;
            savingsAccount.setAccountBalance(accountBalance);
            this.savingsAccountRepository.save(savingsAccount);
            return amount;
        }
        throw new InsufficientBalanceException("Insufficient balance");
    }

    @Transactional
    public void deposit(long accountId, double amount){
        final SavingsAccount savingsAccount = this.savingsAccountRepository
                .findById(accountId).orElseThrow(() -> new IllegalArgumentException(" Invalid Savings account id"));

        double accountBalance = savingsAccount.getAccountBalance();
        savingsAccount.setAccountBalance(accountBalance + amount);
        this.savingsAccountRepository.save(savingsAccount);
    }

    @Transactional
    public Transaction checkBalance(long accountId){
        final SavingsAccount savingsAccount = this.savingsAccountRepository
                .findById(accountId).orElseThrow(() -> new IllegalArgumentException(" Invalid Savings account id"));

        double accountBalance = savingsAccount.getAccountBalance();
        Transaction statement = new Transaction();
        statement.setAmount(accountBalance);
        statement.setEmailAddress(savingsAccount.getCustomer().getEmailAddress());
        return statement;
    }
}