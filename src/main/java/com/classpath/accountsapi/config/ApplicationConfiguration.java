package com.classpath.accountsapi.config;

import com.classpath.accountsapi.util.HelloWorld;
import org.springframework.boot.autoconfigure.condition.ConditionalOnJava;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.system.JavaVersion;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class ApplicationConfiguration {

    @Bean
    @ConditionalOnProperty(prefix = "app", value = "flag", havingValue = "false",  matchIfMissing = true)
    public HelloWorld helloWorld(){
            return new HelloWorld();
    }

    @Bean
    @ConditionalOnMissingBean(name = {"helloWorld"})
    public HelloWorld helloWorld2(){
            return new HelloWorld();
    }

    @Bean
    @ConditionalOnJava(JavaVersion.EIGHT)
    @ConditionalOnMissingBean(name={"helloWorld", "helloWorld2"})
    public HelloWorld helloWorld3(){
            return new HelloWorld();
    }

}

