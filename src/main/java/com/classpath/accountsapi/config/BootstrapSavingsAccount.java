package com.classpath.accountsapi.config;

import com.classpath.accountsapi.model.Address;
import com.classpath.accountsapi.model.Customer;
import com.classpath.accountsapi.model.SavingsAccount;
import com.classpath.accountsapi.repository.CustomerRepository;
import com.github.javafaker.Faker;
import com.github.javafaker.Name;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.HashSet;
import java.util.stream.IntStream;

@Configuration
@RequiredArgsConstructor
public class BootstrapSavingsAccount implements ApplicationListener<ApplicationReadyEvent> {

    private final CustomerRepository customerRepository;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {

        Faker faker = new Faker();

        IntStream.range(1, 2000)
                .forEach((index) -> {
            Customer customer = Customer
                    .builder()
                    .customerName(faker.name().firstName())
                    .dob(LocalDate.of(faker.number().numberBetween(1980, 2000), faker.number().numberBetween(1, 10), faker.number().numberBetween(10, 22)))
                    .pan(faker.idNumber().ssnValid())
                    .addressSet(new HashSet<>())
                    .emailAddress(faker.name().firstName() + "@gmail.com")
                    .build();

            Address communicationAddress = Address.builder()
                    .city(faker.address().city())
                    .state(faker.address().state())
                    .zipCode(faker.address().zipCode())
                    .build();

            Address permanentAddress = Address.builder()
                    .city(faker.address().city())
                    .state(faker.address().state())
                    .zipCode(faker.address().zipCode())
                    .build();

            SavingsAccount savingsAccount = SavingsAccount
                    .builder()
                    .accountBalance(faker.number().numberBetween(26000, 45000))
                    .build();
            customer.addSavingsAccount(savingsAccount);

            customer.addAddress(communicationAddress);
            customer.addAddress(permanentAddress);

            this.customerRepository.save(customer);

        });

//        for(int index = 0; index < 2000; index ++ ){


        //}
    }
}