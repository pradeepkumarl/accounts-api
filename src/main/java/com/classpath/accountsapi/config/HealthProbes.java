package com.classpath.accountsapi.config;

import com.classpath.accountsapi.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.actuate.health.Status;
import org.springframework.context.annotation.Configuration;

import static org.springframework.boot.actuate.health.Status.UP;

@Configuration
@RequiredArgsConstructor
class DBService implements HealthIndicator {

    private final CustomerRepository customerRepository;
    @Override
    public Health health() {
        //perform a read query
        try {
            final long number = this.customerRepository.count();
            return Health.status(UP).withDetail("DB Service", "DB service is UP").build();
        } catch (Exception exception){
            return Health.outOfService().withDetail("DB Service", "DB service is DOWN").build();
        }
    }
}

@Configuration
@RequiredArgsConstructor
class MessageBrokerService implements HealthIndicator {
    @Override
    public Health health() {
        //perform a read query
        return Health.status(UP).withDetail("Kafka Service", "Kafka service is UP").build();
    }
}