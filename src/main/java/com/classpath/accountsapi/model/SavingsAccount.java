package com.classpath.accountsapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import static javax.persistence.GenerationType.AUTO;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
@EqualsAndHashCode(of = {"accountId", "customerName"})
@Entity
@Table(name="savings_account")
public class SavingsAccount {
    @Id
    @GeneratedValue(strategy = AUTO)
    private long accountId;

    @Min(value = 25000, message = "min account balance should be 25000")
    private double accountBalance;

    @OneToOne
    @JoinColumn(name="customer_id")
    @JsonIgnore
    private Customer customer;
}