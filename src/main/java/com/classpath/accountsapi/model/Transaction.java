package com.classpath.accountsapi.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class Transaction {
    @Min(value = 25000, message = "min account balance should be 25000")
    @Max(value = 50000, message="max amount that can be transferred is 50K")
    private double amount;

    @NotEmpty(message = "email address cannot be empty")
    @JsonProperty(value = "email")
    private String emailAddress;
}