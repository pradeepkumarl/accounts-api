package com.classpath.accountsapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import static javax.persistence.GenerationType.AUTO;

@Entity
@Table(name="address")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = {"addressId", "city", "state", "zipCode"})
public class Address {
    @Id
    @GeneratedValue(strategy = AUTO)
    private int addressId;

    private String city;

    private String state;

    private String zipCode;

    @ManyToOne
    @JoinColumn(name="customer_id", nullable = false)
    @JsonIgnore
    private Customer customer;
}