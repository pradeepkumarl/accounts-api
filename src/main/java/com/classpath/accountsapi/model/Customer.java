package com.classpath.accountsapi.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Past;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.FetchType.LAZY;

@Entity
@Table(name="customer")
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
@EqualsAndHashCode(of = {"customerId", "customerName"})
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long customerId;

    @NotEmpty(message = "customer name cannot be empty")
    private String customerName;

    private String username;

    @Past
    private LocalDate dob;

    private String emailAddress;

    private String pan;

    @OneToOne(mappedBy = "customer", cascade = ALL, fetch = EAGER)
    private SavingsAccount savingsAccount;

    @OneToMany(mappedBy = "customer", cascade = ALL, fetch = LAZY)
    private Set<Address> addressSet = new HashSet<>();

    //scaffolding code
    public void addSavingsAccount(SavingsAccount savingsAccount){
        this.setSavingsAccount(savingsAccount);
        savingsAccount.setCustomer(this);
    }

    public void addAddress(Address address){
        this.getAddressSet().add(address);
        address.setCustomer(this);
    }
}