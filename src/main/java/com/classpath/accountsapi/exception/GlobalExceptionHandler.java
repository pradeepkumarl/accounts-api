package com.classpath.accountsapi.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler({IllegalArgumentException.class})
    public ResponseEntity<Error> handleInvalidAccountId(Exception exception){
        return ResponseEntity.status(NOT_FOUND).body(new Error (100, exception.getMessage()));
    }

    @ExceptionHandler({InsufficientBalanceException.class})
    public ResponseEntity<Error> handleInsufficientBalance(Exception exception){
        return ResponseEntity.status(NOT_FOUND).body(new Error (101,exception.getMessage()));
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity<Error> handleInvalidData(Exception exception){
        return ResponseEntity.status(BAD_REQUEST).body(new Error (101,exception.getMessage()));
    }
}

@AllArgsConstructor
@Getter
@Setter
class Error {
    private int code;
    private String message;
}